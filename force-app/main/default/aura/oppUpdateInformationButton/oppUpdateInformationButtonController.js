/**
 * Created by mikhailsapozhnikov on 12/18/20.
 */

({
    closeMethodInAuraController : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

});