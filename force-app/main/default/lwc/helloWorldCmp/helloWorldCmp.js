/**
 * Created by mikhailsapozhnikov on 12/16/20.
 */

import {LightningElement, api, wire, track} from 'lwc';
import {getRecord, getFieldValue, deleteRecord, updateRecord, createRecord} from 'lightning/uiRecordApi';
import getRelatedContacts from '@salesforce/apex/OpportunityCompanyUpdateController.getRelatedContacts'
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {refreshApex} from '@salesforce/apex';
import ACCOUNT_ID_FIELD from '@salesforce/schema/Opportunity.AccountId';
import CONTACT_OBJECT from '@salesforce/schema/Contact';


const columns = [
    {label: 'First Name', fieldName: 'FirstName', editable: true},
    {label: 'Last Name', fieldName: 'LastName', editable: true},
    {label: 'Phone', fieldName: 'Phone', type: 'phone', editable: true},
    {label: 'Email', fieldName: 'Email', type: 'email', editable: true},
    {
        type:  'button-icon',
        initialWidth: 65,
        typeAttributes: {
            iconName: 'utility:delete',
            name: 'delete',
            disabled: false,
        }
    }
]


export default class HelloWorldCmp extends LightningElement {
    columns = columns;
    contactSuccess = true;
    wiredContactResult;
    draftValues = [];
    @track contacts;
    @track error;
    @track displayMessage = '';

    @api recordId;

    @api
    save() {
        this.template.querySelector('lightning-record-edit-form').submit();
        this.handleSave(this.template.querySelector('lightning-datatable').draftValues);
    }

    @api
    getFiredFromAuraSave(event) {
        event.preventDefault();
        const fields = event.detail.fields;
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    @wire(getRecord, {recordId: '$recordId', fields: [ACCOUNT_ID_FIELD]})
    opp;

    @wire(getRelatedContacts, {accId: '$accId'})
    wiredContacts(result) {
        this.wiredContactResult = result;
        if (result.data) {
            this.contacts = result.data;
            this.error = undefined;
        } else if (result.error) {
            console.log("refresh error");
            this.error = result.error;
            this.contacts = undefined;
        }
    }

    get accId() {
        return getFieldValue(this.opp.data, ACCOUNT_ID_FIELD);
    }

    get contactsLength () {
        return this.contacts ? this.contacts.length : false;
    }

    async handleSave(draftValues) {

        const recordInputs = draftValues.slice().map(draft => {
            const fields = Object.assign({}, draft);
            return {fields};
        });
        let promises = new Set();
        for (let i = 0; i < recordInputs.length; i++) {

            if (JSON.stringify(recordInputs[i].fields.Id).includes('row-')) {
                delete recordInputs[i].fields.Id;
                recordInputs[i].fields.AccountId = getFieldValue(this.opp.data, ACCOUNT_ID_FIELD);
                recordInputs[i].apiName = CONTACT_OBJECT.objectApiName;
                promises.add(createRecord(recordInputs[i]))
            } else {
                promises.add(updateRecord(recordInputs[i]));
            }
        }

        Promise.all(promises).then(records => {
            this.draftValues = [];
            this.contactSuccess = true;
            refreshApex(this.wiredContactResult).catch(error => {
                console.log("error in apex refresh ", error);
            })
            this.contacts = this.wiredContactResult.data
        }).catch(error => {
            this.contactSuccess = false;
            let errorBody;
            console.log(JSON.parse(JSON.stringify(error)));
            if(error.body.output.errors.length > 0 ) {
                errorBody = error.body.output.errors[0].message;
            }
            else if (error.body.output.fieldErrors.Name.length > 0) {
                errorBody = error.body.output.fieldErrors.Name[0].message
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating contacts',
                    message: 'Error: ' + errorBody,
                    variant: 'error'
                })
            );
        });
    }

    handleSuccess() {
        console.log("In handleSuccess ", this.contactSuccess);
        if (this.contactSuccess) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Company information updated',
                    variant: 'success'
                })
            );
        }
    }


    handleError(event) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error',
                message: ' Error updating Account information',
                variant: 'error'
            })
        );
    }


    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;

        switch (actionName) {
            case 'delete': {
                if (row.RowIndex != null) {
                    this.contacts = this.contacts.filter(contact => contact.RowIndex !== row.RowIndex);
                    this.contacts.filter(contact => contact.RowIndex != null && contact.RowIndex > row.RowIndex)
                        .forEach(contact => contact.RowIndex = contact.RowIndex - 1);
                    this.draftValues = [];
                    break;
                } else {
                    this.deleteContacts(row);
                    this.template.querySelector('lightning-datatable').draftValues =
                        this.template.querySelector('lightning-datatable').draftValues
                            .filter(draftValue => draftValue.Id !== row.Id);
                    break;
                }
            }
            default:
                return;
        }
    }


    deleteContacts(currentRow) {
        let currentRecord = [];
        currentRecord.push(currentRow.Id);

        deleteRecord(currentRecord[0])
            .then(records => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Contact deleted.',
                        variant: 'success'
                    })
                );
                this.contacts = this.contacts.filter(contact => contact.Id !== currentRow.Id);
            }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error deleting record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }


    addContact() {
        let newContact = {
            Id: null,
            FirstName: null,
            LastName: null,
            Phone: null,
            Email: null,
            RowIndex: this.contacts.length + 1
        }
        this.contacts = [...this.contacts, newContact];
    }


    cancel() {
        this.dispatchEvent(new CustomEvent('close'));
    }

}