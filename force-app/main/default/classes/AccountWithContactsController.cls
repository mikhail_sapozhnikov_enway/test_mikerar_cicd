public with sharing class AccountWithContactsController {

    private final Account account { get; set; }
    public List<Contact> relatedContacts { get; set; }
    public Integer index { get; set; }
    private List<Contact> contactsToDelete = new List<Contact>();


    public AccountWithContactsController(ApexPages.StandardController stdController) {
        this.account = (Account) stdController.getRecord();
        this.relatedContacts = getRelatedContact(account.Id);
        System.debug(this.relatedContacts);

    }

    public void addRow() {
        relatedContacts.add(new Contact());
    }

    public void removeRow() {
        Contact c = relatedContacts.remove(index - 1);
        if (c.Id != null) {
            contactsToDelete.add(c);
        }
    }

    public PageReference save() {

        try {
            upsert account; //TODO - когда создается аккаунт желательно добавить id в параметры страницы, т.к. если перезагрузить страницу пользователь уже не сможет вернуться к редактированию
            for (Contact c : relatedContacts) { //TODO - так легче читается код
                c.AccountId = account.Id;
            }

            upsert relatedContacts;
            delete contactsToDelete;
            contactsToDelete = new List<Contact>(); //TODO - если не очистить после удаления в последующем при нажатии на save будет падать ошибка

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'All saved correctly!'));

        } catch (Exception e) { //TODO - лучше указывать Exception если у тебя один catch и ты хочешь отловить все ошибки
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));


        }
        if (ApexPages.currentPage().getParameters().get('Id') == null) {
            PageReference pageRef = Page.AccountWithContacts;
            pageRef.getParameters().put('id',account.id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;

    }

    public PageReference goToAcc() {
        PageReference acctPage = new ApexPages.StandardController(account).view();
        acctPage.setRedirect(true);
        return acctPage;
    }

    
    private List<Contact> getRelatedContact(Id accId) {
        if (accId != null) {
            return [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :accId];
        }
        return new List<Contact>();
    }
}