/**
 * Created by mikhailsapozhnikov on 12/16/20.
 */

public with sharing class OpportunityCompanyUpdateController {

    @AuraEnabled(cacheable=true)
    public static Account getOppOwner(Id oppId) {
        System.debug(oppId);
        List<Account> accounts = [
                SELECT Id, BillingStreet, BillingPostalCode, BillingState, BillingCountry
                FROM Account
                WHERE Account.Id IN (SELECT AccountId FROM Opportunity WHERE Id = :oppId)
                LIMIT 1];
        Account account = (accounts.size() == 1) ? accounts.get(0) : null;
        System.debug(account);
        return account;
    }


    @AuraEnabled(cacheable=true)
    public static List<Contact> getRelatedContacts(String accId) {
        return [
                SELECT AccountId, Id, FirstName, LastName, Phone, Email
                FROM Contact
                WHERE AccountId = :accId
                WITH SECURITY_ENFORCED
        ];
    }


}